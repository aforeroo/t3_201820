package model.vo;

import java.util.Date;

public class VOStation implements Comparable<VOStation>{

	private int id;
	
	private String name;
	
	private Double latitude;
	
	private Double longitude;
	
	private int dpCapacity;
	
	private Date onlineDate;
	
	
	public VOStation(int pId, String pName, Double pLatitude, Double pLongitude, int pDpCapacity, Date pOnlineDate)
	{
		id = pId;
		name = pName;
		latitude = pLatitude;
		longitude = pLongitude;
		dpCapacity = pDpCapacity;
		onlineDate = pOnlineDate;		
	}


	public int getId() {
		return id;
	}


	public String getName() {
		return name;
	}


	public Double getLatitude() {
		return latitude;
	}


	public Double getLongitude() {
		return longitude;
	}


	public int getDpCapacity() {
		return dpCapacity;
	}


	public Date getOnlineDate() {
		return onlineDate;
	}
	
	
	public String toString()
	{
		return " id:" + id + "  |  name:" + name; 
	}



	public int compareTo(VOStation o) {
		int i = -1;
		if(id == o.id)
			i = 0;
		return i;
	}
	
	

	
	
	
}
