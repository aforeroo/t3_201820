package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip> {

	

	private String tripId;
	
	private Date startTime;
	
	private Date endTime;
	
	private Integer bikeId;
	
	private int tripDuration; //Seconds
	
	private int fromStationId;
	
	private String fromstationName;
	
	private int toStationId;
	
	private String toStationName;
	
	private String userType;
	
	private String gender;
	
	private String birthyear;
	
	
	
	public VOTrip(String tripId, Date startTime, Date endTime, Integer bikeId,
			int tripDuration, int fromStationId, String fromstationName,
			int toStationId, String toStationName, String userType, String gender,
			String birthyear) {
		super();
		this.tripId = tripId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromstationName = fromstationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.userType = userType;
		this.gender = gender;
		this.birthyear = birthyear;
		
	}

	public String getTripId() {
		return tripId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTripSeconds() {
		return tripDuration;
	}

	public int getFromStationId() {
		return fromStationId;
	}

	public String getFromstationName() {
		return fromstationName;
	}

	public int getToStationId() {
		return toStationId;
	}

	public String getToStationName() {
		return toStationName;
	}

	public String getUserType() {
		return userType;
	}

	public String getGender() {
		return gender;
	}

	public String getBirthyear() {
		return birthyear;
	}
	
	public String toString()
	{
		return "bykeId:" + bikeId + " | from: " + fromStationId + " | to: " + toStationId  ;
	}

	
	public int compareTo(VOTrip o) {
		int i = -1;
		if(tripId.equals(o.tripId))
			i = 0;
		return i;
	}

	
	
	
	
	

	
	
	
	
	
	
}
