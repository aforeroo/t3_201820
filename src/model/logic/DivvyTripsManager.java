package model.logic;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import com.univocity.parsers.csv.*;
import api.IDivvyTripsManager;
import model.vo.*;
import model.data_structures.IIteratorListaDoble;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.Queue;
import model.data_structures.Stack;



public class DivvyTripsManager implements IDivvyTripsManager {

	public static final char SEPARATOR=';';
	public static final char QUOTE='"';
	
	
	/**
	 * contiene los trips de mas antiguo a mas reciente
	 * tope de la pila es el mas antiguo
	 */
	public Stack<VOTrip> tripsStack = new Stack<>();
	
	
	/**
	 * contiene los trips de mas reciente a mas antiguo 
	 * el comiezo de la cola tiene al mas reciente 
	 * el final de la cola tiene al mas antiguo
	 */
	public Queue<VOTrip> tripsQueue = new Queue<>();
	
	public ListaDobleEncadenada<VOStation> stationList = new ListaDobleEncadenada<>();
	
	
	
	
	public void loadStations (String stationsFile) {
		
		CsvParserSettings settings = new CsvParserSettings();
		
		CsvParser parser = new CsvParser(settings);
		
		parser.beginParsing(new File(stationsFile));
		
		String[] campos = parser.parseNext();
		long inicio = System.currentTimeMillis();
		while ((campos = parser.parseNext()) != null)
		{
			
			VOStation actual = parserStation(campos);
			stationList.addEnd(actual);
			
		}
		long fin = System.currentTimeMillis();
		System.out.println("\n\n CARGA EXITOSA !!" );
		
		double tiempo = ((double)(fin-inicio)/(1000));
		System.out.println("\n\n  " + stationList.size() + " registros cargados en: " + tiempo + " segundos|| velocidad promedio: " + ((double)(stationList.size())/(tiempo)) + " Lineas/segundo\n\n" );
		
		
		
	}
	
	
	
	
	
	public void loadTrips (String tripsFile) {
	
		CsvParserSettings settings = new CsvParserSettings();
		
		CsvParser parser = new CsvParser(settings);
		
		parser.beginParsing(new File(tripsFile));
		String[] campos = parser.parseNext();
		long inicio = System.currentTimeMillis();
		while ((campos = parser.parseNext()) != null)
		{
			
			VOTrip actual = parserTrips(campos);
			
			tripsQueue.queue(actual);
			tripsStack.push(actual);
		}
		long fin = System.currentTimeMillis();
		System.out.println("\n\n CARGA EXITOSA !!" );
		double tiempo = ((double)(fin-inicio)/(1000));
		System.out.println("\n\n  " + tripsQueue.size() + "	registros cargados en: " + tiempo + " segundos || velocidad promedio: " + ((double)(tripsQueue.size())/(tiempo)) + " Lineas/segundo\n\n" );
		
	}
	
	
	
	
	
	
	/**
	 * usando la cola(Queue) de trips retorna las ultimas n difrenetes estaciones en la que una bicicleta 
	 * con id: bicycleId ha estado
	 */
	public Queue<String> getLastNStations (int bicycleId) throws NoSuchElementException {
		
		ListaDobleEncadenada<VOTrip> tripsByke = cargarTripsAsociados(bicycleId);
		if(tripsByke.isEmpty())
		{
			throw new NoSuchElementException();
		}
		int contadorTrips = tripsByke.size();
		
		//REGLA:
				//se agrega a la lista estaciones solo si  FromStation != toStation
				// y si la estacion toStation anterior es diferente de FromStation actual		
		
		
		//lista de los viajes asociados a esa bicicleta			
		Queue<String> estaciones = new Queue<>();		
			
		IIteratorListaDoble<VOTrip> iterador = tripsByke.iterator();		
		VOTrip actual = iterador.getCurrent();
		
		while(iterador.hasNext())
		{
			int fromId = actual.getFromStationId();
			String fromName = actual.getFromstationName();
			
			int toId = actual.getToStationId();
			String toName = actual.getToStationName();
			
			if(!iterador.hasPrev())
			{
				estaciones.queue(" (" + fromId + ")  " + fromName);
				
				if(fromId != toId)
				{
					estaciones.queue(" (" + toId + ")  " + toName);
				}				
				
			}else
			{
				int prevToId = iterador.getprev().getToStationId();
				
				
				if(fromId != prevToId)
				{
					estaciones.queue(" (" + fromId + ")  " + fromName);
				}
				if(fromId != toId)
				{
					estaciones.queue(" (" + toId + ")  " + toName);
				}
				
			}
			
			actual = iterador.next();
			
			
			
		}
		
		int contadorEstaciones = estaciones.size();	
		
		
		System.out.println("\n\tla bicicleta con id: " + bicycleId + " tiene asociados " + contadorTrips + " viajes y se ha movido " + contadorEstaciones + " veces entre estaciones" );
		
		
		
		return estaciones;
		
		
		
		
	}
	
	
	
	public ListaDobleEncadenada<VOTrip> cargarTripsAsociados(int bikeId)
	{
		ListaDobleEncadenada<VOTrip> tripsByke = new ListaDobleEncadenada<>();		
		
		
		IIteratorListaDoble<VOTrip> iterador = tripsQueue.iterator();
		VOTrip actual = iterador.next();
		
		
		while(iterador.hasNext())
		{
			if(actual.getBikeId()==bikeId)
			{
				tripsByke.addEnd(actual);				
			}
			actual = iterador.next();
		}
		
		return tripsByke;
	}
	
	
	

	
	
	/**
	 * usa la pila(Stack) de trips para mostrar la informacion del trip numero n que termino en la estacion con
	 * identificador entero stationID
	 */

	
	public Stack<VOTrip> cargarTripsEndstation(int stationID)
	{
		Stack<VOTrip> trips = new Stack<>();
		
		IIteratorListaDoble<VOTrip> iterador = tripsStack.iterator();
		VOTrip actual = iterador.getCurrent();
		while(iterador.hasNext())
		{
			if(actual.getToStationId() == stationID)
			{
				trips.push(actual);
				
			}
			actual = iterador.next();
		}

		
		return trips;
		
	}
	
	
	
	//parsers & utlities
	
	
	
	public static Date convertDateStations(String str)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = null;
		try {
			date = formatter.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	
	
	
	
	
	public static Date convertDateTrips(String str)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = null;
		try {
			date = formatter.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	
	
	
	public static VOStation parserStation(String[] campos)
	{
		VOStation actualStation;			
		actualStation = new VOStation(Integer.valueOf(campos[0]), campos[1], Double.valueOf(campos[3]),
									  Double.valueOf(campos[4]), Integer.valueOf(campos[5]),
									  convertDateStations(campos[6]));		
		
		System.out.println( actualStation.getId() + " || " + actualStation.getName());
		return actualStation;
	}
	
	
	
		
	
	public static VOTrip parserTrips(String[] campos)
	{
		
		VOTrip actualTrip;
		
		String userType = campos[9];
		String gender = "";
		String birthYear = "";
		if(userType.equals("Subscriber"))
		{
			try {
				if(!campos[10].equals("") || !campos[10].equals(null))
					gender = campos[10];
				if(!campos[11].equals("") || !campos[10].equals(null))
					birthYear = campos[11];
			} catch (Exception e) {
				
			}
			
		}
		
		actualTrip = new VOTrip(campos[0], convertDateTrips(campos[1]), convertDateTrips(campos[2]),
								Integer.valueOf(campos[3]), Integer.valueOf(campos[4]), Integer.valueOf(campos[5]), campos[6],
								Integer.valueOf(campos[7]), campos[8], userType, gender, birthYear);
		System.out.println(actualTrip.getTripId());
		return actualTrip;
	}
	
	


}
