package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;






public class Lista<E> implements ILista<E>{

	
	
	
	Nodo<E> primero;
	
	Nodo<E> ultimo;
	
	int size;
	
	public Lista()
	{
		primero = ultimo = null;
		size = 0;
	}	

	
	public boolean isEmpty() {
		
		return primero == null;
	}

	
	public int size() {
		
		return size;
	}
	
	
	public void addFirst(E item)
	{
		if(isEmpty())
			primero = ultimo = new Nodo<E>(item);
		else
			primero = new Nodo<E>(item, primero);
	}
	
	
	
	public void addEnd(E item)
	{
		if(isEmpty())
			primero = ultimo = new Nodo<E>(item);
		else
			ultimo = ultimo.next = new Nodo<E>(item);
	}
	
	public E removeFirst()
	{
		if(isEmpty())
			throw new NoSuchElementException();
		E antiguoPrimero = primero.item;
		
		if(primero == ultimo)
			primero = ultimo = null;
		else
			primero = primero.next;
		
		
		return antiguoPrimero;		
	}
	
	public E removeEnd()
	{
		if(isEmpty())
			throw new NoSuchElementException();
		
		E antiguoUltimo = ultimo.item;
		
		if(primero == ultimo)
			primero = ultimo = null;
		else{
			
			Nodo<E> actual = primero;
			
			while(actual.next != ultimo)
			{
				actual = actual.next;
			}
			
			ultimo = actual;
			actual.next = null;
				
		}
		
		return antiguoUltimo;
		
	}
	
	public void contains(E item)
	{
		
	}
	
	
	
	
	/**
	 * elimina el ultimo elemento sin retornarlo
	 */
	public void removeLast()
	{
		E temp = removeEnd();
		System.out.println("Ultimo objeto removido: " + temp.toString());
	}
	
	
	/**
	 * elimina el primer elemento sin retornarlo
	 */
	public void removeBeginning()
	{
		E temp = removeFirst();
		System.out.println("Primer objeto removido: " + temp.toString());
	}
	
	
	
	public void print()
	{
		IteradorLista<E> iterador = iterator();
		E actual = iterador.next();
		while(iterador.hasNext())
		{
			System.out.println(actual.toString());
			actual = iterador.next();
		}
	}
	
	public void delete()
	{
		primero = null;
		ultimo = null;
	}
	
	
	
	
	public IteradorLista<E> iterator() {
		
		return new IteradorLista<E>(primero);
	}

	
	public class IteradorLista<T> implements Iterator<T>
	{
		Nodo<T> current;
		int index;
		
		public IteradorLista(Nodo<T> first)
		{
			current = first;
			index = 1;
		}
		
		public boolean hasNext() {
			
			return current.next != null;
		}

		
		public T next() {
			
			if(!hasNext()) throw new NoSuchElementException();
			
			T item = current.next.item;
			current = current.next;
			index++;
			return item;
		}
		
		
				
		
		public void remove()
		{
			Nodo<T> oldCurrent = current;			
			current = oldCurrent.next;		
			oldCurrent = null;
			index--;
			
		}
		
		public E getFirst()
		{
			return primero.item;
		}
		
		public E getLast()
		{
			return ultimo.item;
		}
		
		
	}

		



}
