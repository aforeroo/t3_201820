package model.data_structures;

public class Queue<E> extends ListaDobleEncadenada<E>{

	NodoDoble<E> primero;
	
	NodoDoble<E> ultimo;
	
	int size;
	
	
	
	public Queue()
	{
		super();
	}	
	
	
	public void queue(E item)
	{
		super.addEnd(item);
	}
	
	public E unQueue()
	{
		return super.removeFirst();
	}
	
	
	
}
