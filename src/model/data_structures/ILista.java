package model.data_structures;

public interface ILista<E> extends Iterable<E>{

	public boolean isEmpty();
	
	public int size();
	
	public void addFirst(E item);
	
	public void addEnd(E item);
	
	public E removeFirst();
	
	public E removeEnd();
	
	public void print();
	
	public void delete();
	
}
