package model.data_structures;

import java.util.Iterator;

public interface IIteratorListaDoble<E> extends Iterator<E>{

	public E getCurrent(); 
	
	public E getNext();
		
	public boolean hasPrev();
	
	public E prev();
	
	public E getprev();
	
	public E removeCurrent();
	
	public E prevPrev();
	
	
	
	
	
	
}
