package model.data_structures;


import java.util.NoSuchElementException;





public class ListaDobleEncadenada<E> implements ILista<E> {

	NodoDoble<E> primero;
	
	NodoDoble<E> ultimo;
	
	int size;
	
	
	
	
	
	
	
	public ListaDobleEncadenada()
	{
		primero = ultimo = null;
		size = 0;
	}
	
	
	
	
	
	
	
	public boolean isEmpty() {
		
		return primero==null;
	}
	
	
	
	
	
	
	
	
	public int size() {
		
		return size;
	}
	
	public E getFirst()
	{
		return primero.item;
	}
	
	public E getLast()
	{
		return ultimo.item;
	}
	
	
	
	

	
	public void addFirst(E nuevo) {
		
		if(isEmpty())
			primero = ultimo = new NodoDoble<E>(nuevo);
		else
			primero = new NodoDoble<E>(nuevo, null, primero);
		
		size++;
	}
	
	
	

	
	public void addEnd(E item) {
		
		if(isEmpty())
			primero = ultimo = new NodoDoble<E>(item);
		else{
			NodoDoble<E> antiguoUltimo = ultimo;
			ultimo = ultimo.next = new NodoDoble<E>(item, antiguoUltimo , null);
		}
		size++;
	}	
	
	
	
		
	
	public E removeFirst(){

		if(isEmpty())
			throw new NoSuchElementException();
		
		E data = primero.item;
		
		if(primero == ultimo)
			primero = ultimo = null;
		else{
			primero = primero.next;
			primero.prev = null;		
		}		
		
		return data;
	}
	
	
	
	
	
	
	
	public E removeEnd()
	{

		if(isEmpty())
			throw new NoSuchElementException();
		
		E data = ultimo.item;
		
		if(primero == ultimo)
			primero = ultimo = null;
		else{
			ultimo = ultimo.prev;
			ultimo.next = null;
		}	
		
		return data;
		
	}
	
	
	
	public E getItemk(int k)
	{
		E item = null;
		if(k <= size)
		{
			int contador = 0;
			IteradorListaDoble<E> iterador = iterator();
			E actual = iterador.getCurrent();
			while(contador < k)
			{
				actual = iterador.next();
				contador++;
			}
			item = actual;
		}
		return item;
	}
	
	
	public void delete()
	{
		primero = null;
		ultimo = null;
	}
	
	
	
	
	
	public void print()
	{
		IteradorListaDoble<E> iterador = iterator();
		E actual = iterador.getCurrent();
		while(iterador.hasNext())
		{
			System.out.println(actual.toString());
			actual = iterador.next();
		}
		System.out.println(actual.toString());
	}
	
	 

	
	

	
	public IteradorListaDoble<E> iterator() {		
		return new IteradorListaDoble<>(primero);
	}
	
	
	
	
	
	
	public class IteradorListaDoble<T> implements IIteratorListaDoble<T>
	{

		private NodoDoble<T> current;
		
		
		
		//constructor Iterador
		public IteradorListaDoble(NodoDoble<T> first)
		{
			current = first;			
		}
		
		public T getCurrent()
		{
			return current.item;
		}
				
		
		public boolean hasNext() {
			
			return current.next != null;
		}
		
		public boolean hasPrev()
		{
			return current.prev != null;
		}

		
		public T next() {
						 
			if(!hasNext()) throw new NoSuchElementException();
			
			T item = current.next.item;			
			current  = current.next;
			
			
			return item;
		}
		
		public T prev(){
			
			if(!hasPrev()) throw new NoSuchElementException();
			
			T item = current.prev.item;
			current= current.prev.prev;
			
			return item;
		}
		
		public T removeCurrent()
		{
			NodoDoble<T> oldCurrent = current;
			T item = oldCurrent.item;
						
			current = oldCurrent.prev;
			current.next = oldCurrent.next;
			oldCurrent = null;
			
			return item;
			
		}




		
		public T getNext() {
			
			return current.next.item;
		}




		public T prevPrev()
		{
			return current.prev.prev.item;	

		}
		
	
		public T getprev() {
			return current.prev.item;
		}



		
		
	}

	

}
