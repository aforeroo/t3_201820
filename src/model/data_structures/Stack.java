package model.data_structures;



public class Stack<E> extends ListaDobleEncadenada<E>{

	NodoDoble<E> primero;
	
	NodoDoble<E> ultimo;
	
	int size;
	
	public Stack()
	{
		super();
	}
	
	public void push(E item)
	{
		addEnd(item);
	}
	
	public E pop()
	{
		return removeEnd();
	}
	
	public E peek()
	{
		return ultimo.item;
	}

}
