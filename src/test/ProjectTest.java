package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.DivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;



public class ProjectTest {

	Queue<Integer> queue;
	
	Stack<Integer> stack;
	
	DivvyTripsManager manager;
	
	
	
	
	public void load()
	{
		manager = new DivvyTripsManager();
		manager.loadTrips("./test/SmallTripsCSV.csv");
		manager.loadStations("./test/SmallStationsCSV.csv");
	}
	
	@Before
	public void beforeQueue()
	{
		queue = new Queue<Integer>();
		
	}
	
	@Before
	public void beforeStack()
	{
		stack = new Stack<Integer>();
		
	}
	
	
	@Test
	public void testQueue() {
		queue.queue(1);
		Integer esperado = 1;
		assertEquals(esperado, queue.getLast());
		queue.queue(2);
		queue.queue(3);
		esperado = 3;
		assertEquals(esperado, queue.getLast());
	}
	
	@Test
	public void testUnQueue() {
		queue.queue(0);
		queue.queue(1);		
		Integer esperado = 0;		
		assertEquals(esperado, queue.unQueue());		
	}
	
	
	@Test
	public void testStackPush() {
		stack.push(0);
		stack.push(1);
		Integer esperado = 0;
		assertEquals(esperado, stack.getFirst());
	}
	
	@Test
	public void testStackPop() {
		stack.push(0);
		stack.push(1);
		Integer esperado = 1;
		assertEquals(esperado, stack.pop());
		
	}
	
	@Test
	public void testLoadTrips() {
		
		load();
		
		VOTrip primero = manager.tripsQueue.unQueue();
		String expectedIdPrimero = "16734065";
		assertEquals(expectedIdPrimero, primero.getTripId());
		
		VOTrip ultimo = manager.tripsQueue.getLast();
		String expectedIdUltimo = "16734062";
		assertEquals(expectedIdUltimo, ultimo.getTripId());		
	
	}
	
	
	
	@Test
	public void testLoadStations() {
		
		load();
		
		VOStation primero = manager.stationList.getFirst();
		String expectedName = "Buckingham Fountain";
		assertEquals(expectedName, primero.getName());
		
		
		VOStation ultimo = manager.stationList.getLast();
		String expectedName2 = "Burnham Harbor";
		assertEquals(expectedName2, ultimo.getName());		
		
	}
	
	
	
	

}
