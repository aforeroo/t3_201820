package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadStations();
					break;
					
				case 2:
					Controller.loadTrips();
					break;
					
				case 3:
					System.out.println("Ingrese el id de la bicileta:");
					int bicycleId = Integer.parseInt(sc.next());
					try{
						long ini = System.currentTimeMillis();
						Queue<String> lastStations = Controller.getLastNStations (bicycleId);
						long fini = System.currentTimeMillis();
						
						System.out.println("\n\tresultados encontrados en " + ((double)(fini-ini)/1000) + " segundos \n");
						System.out.println("\tno se consideran los viajes que volvieron a la misma estacion");
						System.out.println("\nIngrese en numero de viajes:  (1 <= n <= " + lastStations.size()+ ") : ");
						
						int numberOfTrips = Integer.parseInt(sc.next());
					
						
						System.out.println("Las ultimas " + numberOfTrips + " estaciones: \n\n");
						
						int contador = 1;

						while(contador < numberOfTrips)
						{
							System.out.println(" " +contador+ ". "  + lastStations.unQueue()  );
							System.out.println(" | ");
							System.out.println(" | to");
							System.out.println(" | ");
							System.out.println("\\|/");					

							contador++;
						}
						System.out.println(" " + (contador) + ". " + lastStations.unQueue() + "\n\n"  );
					}catch(Exception e)
					{
						System.out.println("no existe la bicicleta con ese id o no tiene viajes registrados1");
					}
					
					
								
					
					break;
					
				case 4:
					System.out.println("Ingrese el identificador de la estacion:");
					int stationId = sc.nextInt();
					
					long ini2 = System.currentTimeMillis();
					Stack<VOTrip> trips = Controller.cargarTripsEndstation(stationId);
					long fini2 = System.currentTimeMillis();
					
					
					
					int size = trips.size();
					System.out.println("\n\t" + size + " viajes asociados a la estacion " + stationId );
					System.out.println("\n\tresultados encontrados en " + ((double)(fini2-ini2)/1000) + " segundos\n");
					
					System.out.println("Ingrese el numero del viaje que se quiere buscar: (1 <= n <= " + size + " ) :"  );
					int nTrip = Integer.parseInt(sc.next());
					
					
					VOTrip actual = trips.getItemk(nTrip); 
					
					
					
				
					if(actual == null) {
						System.out.println("\n" + "No existe el viaje " + nTrip);
						
					} else {
						System.out.println("\n" +actual.getTripId() + " (" + actual.getTripSeconds() + "): " + actual.getFromstationName() + "-->" + actual.getToStationName() + "\n\n");
					}
					
					break;
					
				case 5:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("------------ANDRES FORERO: 20161434----------------\n");
		System.out.println("1. Cree una nueva coleccion de estaciones");
		System.out.println("2. Cree una nueva coleccion de viajes");
		System.out.println("3. Dar ultimas estaciones por las que ha pasado la bicicleta");
		System.out.println("4. Dar viaje numero N que llega a la estacion");
		System.out.println("5. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
