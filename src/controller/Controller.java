package controller;

import api.IDivvyTripsManager;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {


	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		manager.loadStations("./docs/Divvy_Stations_2017_Q3Q4.csv");
	}
	
	public static void loadTrips() {
		manager.loadTrips("./docs/Divvy_Trips_2017_Q3.csv");
	}
		
	public static Queue<String> getLastNStations (int bicycleId) {
		return manager.getLastNStations(bicycleId);
	}
	
	public static Stack<VOTrip> cargarTripsEndstation(int stationID) {
		return manager.cargarTripsEndstation(stationID);
	}
}
